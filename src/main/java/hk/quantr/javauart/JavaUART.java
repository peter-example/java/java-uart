package hk.quantr.javauart;

import com.fazecast.jSerialComm.SerialPort;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class JavaUART {

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss.s");

	public static void main(String args[]) {
		SerialPort[] ports = SerialPort.getCommPorts();
		for (SerialPort port : ports) {
			System.out.println(port + ", " + port.getSystemPortName() + ", " + port.getSystemPortPath());
		}

		SerialPort comPort = SerialPort.getCommPorts()[0];
		comPort.setBaudRate(115200);
		comPort.setNumDataBits(8);
		comPort.setParity(SerialPort.NO_PARITY);
		comPort.setNumStopBits(SerialPort.ONE_STOP_BIT);
		comPort.openPort();
//		comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 100, 0);

		System.out.println(sdf.format(new Date()));

		int count = 0;
		StringBuilder str = new StringBuilder();
		try {
			while (true) {
				byte bytes[] = "Peter".getBytes();
				comPort.writeBytes(bytes, bytes.length);
//				while (comPort.bytesAvailable() == 0) {
//					System.out.println("sleep");
//					Thread.sleep(20);
//				}
//				comPort.flushIOBuffers();

				byte[] readBuffer = new byte[comPort.bytesAvailable()];
				int numRead = comPort.readBytes(readBuffer, readBuffer.length);
				//System.out.println("Read " + numRead + " bytes.");//
//				for (byte b : readBuffer) {
//					System.out.print((char) b);
//				}
//				str.append(new String(readBuffer));
				count += numRead;
				if (count >= 100000) {
					System.out.println(count);
//					System.out.println(str);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(sdf.format(new Date()));
		comPort.closePort();
	}
}
